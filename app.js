const express = require('express')
const mongoose = require('mongoose') 

// this allows us to use all the routes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute')

const app = express()
const port = 3001

app.use(express.json())
app.use(express.urlencoded({extended:true}))



mongoose.connect(
  "mongodb+srv://admin:admin1234@zuitt-bootcamp.vhxdp.mongodb.net/s31?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use('/tasks', taskRoute)


app.listen(port, ()=> console.log(`listening to port ${port}`))

//console.log(app)