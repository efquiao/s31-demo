const express = require('express');
const task = require('../models/task');
const router = express.Router();
const taskController = require('../controllers/taskController')

//Routes to ge all tasks
router.get('/',(req,res) => {
  taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

router.post('/', (req,res)=>{
  taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController))
})



router.get('/:id', (req,res)=>{
 // res.send(req.params.id)
 taskController.getTask(req.params.id)
})

// use "module.exports" to export the router to use in the "app.js"
module.exports = router;